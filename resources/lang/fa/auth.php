<?php

return [
    'admin-failed' => 'نام کاربری یا رمز عبور اشتباه است.',
    'admin-throttle' => 'برای تلاش دوباره باید :seconds ثانیه دیگر مراجعه کنید.',
    'admin-status' => 'وضعیت کاربری شما غیر فعال است.',
    'pool-admin-failed' => 'نام کاربری یا رمز عبور اشتباه است.',
    'pool-admin-throttle' => 'برای تلاش دوباره باید :seconds ثانیه دیگر مراجعه کنید.',
    'pool-admin-status' => 'وضعیت کاربری شما غیر فعال است.',
    'operator-failed' => 'نام کاربری یا رمز عبور اشتباه است.',
    'operator-throttle' => 'برای تلاش دوباره باید :seconds ثانیه دیگر مراجعه کنید.',
    'operator-status' => 'وضعیت کاربری شما غیر فعال است.',
    'front-failed' => 'نام کاربری یا رمز عبور اشتباه است.',
    'front-throttle' => 'برای تلاش دوباره باید :seconds ثانیه دیگر مراجعه کنید.',
    'front-status' => 'وضعیت کاربری شما غیر فعال است.',
    'organization-status' => 'وضعیت کاربری شما غیر فعال است.',
    'organization-failed' => 'نام کاربری یا رمز عبور اشتباه است.',
    'organization-throttle' => 'برای تلاش دوباره باید :seconds ثانیه دیگر مراجعه کنید.',
];
