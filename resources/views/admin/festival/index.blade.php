@extends('admin.layout.main')

@section('title', 'لیست جشنواره‌ها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">جشنواره‌ها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست جشنواره‌ها</div>
            <div class="panel-body">
                <p class="text-left"><a class="btn btn-success" href="{{action('Admin\FestivalController@getCreate')}}"><i class="fa fa-plus"></i> جشنواره جدید</a></p>
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif

                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>عنوان</th>
                            <th>کد جشنواره</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($festivals as $festival)
                        <tr>
                            <td>{{$festival->festival_id}}</td>
                            <td>{{$festival->name}}</td>
                            <td class="en">{{$festival->codename}}</td>
                            <td><span class="label label-{{checkStatus($festival->status, 'success', 'danger')}}">{{checkStatus($festival->status, 'فعال', 'غیر فعال')}}</span></td>
                            <td>
                                <a href="{{action('Admin\FestivalController@getUpdate', $festival->festival_id)}}" class="btn btn-info btn-xs" title="ویرایش"><i class="fa fa-pencil"></i></a>
                                <a href="{{action('Admin\FestivalController@getDestroy', $festival->festival_id)}}" class="btn btn-danger btn-xs confirm" title="حذف"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $('.custom-datatable').DataTable( {
        responsive: true,
        language: window.datatableLanguage,
        order: [[ 0, "desc" ]],
    });
</script>
@stop
