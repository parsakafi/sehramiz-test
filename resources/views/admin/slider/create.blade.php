@extends('admin.layout.main')

@section('title', 'ایجاد اسلایدر جدید')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">اسلایدر‌</h1>
            <div class="panel panel-default">
                <div class="panel-heading">ایجاد اسلایدر جدید
                    <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\SliderController@getIndex')}}"
                       title="برگشت"><i class="fa fa-reply"></i></a>
                </div>
                <div class="panel-body">
                    <form action="{{action('Admin\SliderController@postCreate')}}" method="post"
                          enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>عنوان اسلایدر @required</label>
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('name', '', ['class' => 'form-control', 'required']) !!}
                                {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>تصاویر</label>
                            </div>
                            <div class="slider_item clearfix">
                                <div class="col-md-6">
                                    {!! Form::text('image_title[]', '', array('class' => 'form-control image_title','required' => 'required','placeholder'=>'عنوان')) !!}
                                </div>
                                <div class="col-md-5">
                                    <input name="image_file[]" type="file" class="form-control image_file" required>
                                </div>
                                <div class="col-md-1">
                                    <a href="#" class="remove_slider_item btn btn-danger hidden"><i
                                                class="fa fa-trash fa-fw"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <a href="#" class="add_slider_item btn btn-success"><i class="fa fa-plus fa-fw"></i></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>وضعیت</label>
                            </div>
                            <div class="col-md-2">
                                <label>غیر فعال</label>
                                {!! Form::radio('status', '0', false, ['class' => 'icheck-status disable']) !!}
                                <label>فعال</label>
                                {!! Form::radio('status', '1', true, ['class' => 'icheck-status enable']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ csrf_field() }}
                            <div class="col-md-6 text-right">
                                <a href="{{action('Admin\SliderController@getIndex')}}"
                                   class="btn btn-default">برگشت</a>
                            </div>
                            <div class="col-md-6 text-left">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> ایجاد کن
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script>
        $(function () {
            $('.clear-addon-parent').change(function () {
                if ($(this).is(':checked')) {
                    $(this).closest('.input-group').find('.clear-addon').val('');
                }
            });
            $('.clear-addon').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).next('.input-group-addon').find('.clear-addon-parent').prop('checked', false);
                }
            });
        });
    </script>
@stop
