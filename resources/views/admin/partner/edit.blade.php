@extends('admin.layout.main')

@section('title', 'ویرایش همکار')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">همکار‌ها
            @include('admin.partner.navigation')
        </h1>
        <div class="panel panel-default">
            <div class="panel-heading">ویرایش همکار
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\PartnerController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <form action="{{action('Admin\PartnerController@postUpdate')}}" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام @required</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('name', $partner->name, ['class' => 'form-control', 'required']) !!}
                            {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام خانوادگی</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('last_name', $partner->last_name, ['class' => 'form-control']) !!}
                            {!! $errors->first('last_name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>ایمیل</label>
                        </div>
                        <div class="col-md-5">
                            {!! Form::text('email', $partner->email, ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('email', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>موبایل</label>
                        </div>
                        <div class="col-md-3">
                            {!! Form::text('mobile', $partner->mobile, ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('mobile', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام کاربری @required</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('username', $partner->username, ['class' => 'form-control en ltr', 'required']) !!}
                            {!! $errors->first('username', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>رمز عبور</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::password('password', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-warning">اگر مایل به تغییر رمز نیستید، این فیلد را خالی رها کنید.</p>
                            <p class="text-warning">حداقل طول رمز 8 کاراکتر</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>تایید رمز عبور</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::password('password_confirmation', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password_confirmation', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>حداقل میزان اعتبار @required</label>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                {!! Form::text('minimum_credit', $partner->minimum_credit, ['class' => 'form-control en ltr p-sep-ltr', 'required']) !!}
                                <div class="input-group-addon">ریال</div>
                            </div>
                            {!! $errors->first('minimum_credit', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>آواتار</label>
                        </div>
                        <div class="col-md-5">
                            <input name="avatar" type="file" class="form-control">
                            @if ($partner->avatar)
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="none-begard-remove-selected" data-name="avatar_removed" data-value="1">
                                        <li>
                                            <span class="remove"><a class="btn btn-xs btn-warning" href="javascript:void(0)"><i class="fa fa-times"></i></a></span>
                                            <span class="image"><img class="img-thumbnail" src="{{URL::asset(Manage::uploadDir('partners').$partner->partner_id.'.'.$partner->avatar)}}"></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endif
                            {!! $errors->first('avatar', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>آی‌پی‌ها</label>
                        </div>
                        <div class="col-md-5">
                            {!! Form::textarea('ip', $ip, ['class' => 'form-control en ltr']) !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-warning">هر IP در یک سطر</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>وضعیت</label>
                        </div>
                        <div class="col-md-2">
                            <label>غیر فعال</label>
                            {!! Form::radio('status', '0', $partner->status == 0 ? true : false, ['class' => 'icheck-status disable']) !!}
                            <label>فعال</label>
                            {!! Form::radio('status', '1', $partner->status == 1 ? true : false, ['class' => 'icheck-status enable']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="partner_id" value="{{$partner->partner_id}}">
                        <div class="col-md-6 text-right">
                            <a href="{{action('Admin\PartnerController@getIndex')}}" class="btn btn-default">برگشت</a>
                        </div>
                        <div class="col-md-6 text-left">
                            <button type="submit" class="btn btn-info"><i class="fa fa-pencil"></i> ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
