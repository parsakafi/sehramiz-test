@extends('admin.layout.main')

@section('title', 'اعتبارات همکاران')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اعتبارات همکاران</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                اعتبارات همکاران
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\PartnerCreditController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th class="active">آی‌دی رویداد</th>
                            <td class="en">{{$creditHolder->partner_credit_event_id}}</td>

                            <th class="active">نوع رویداد</th>
                            <td><span>{{$creditHolder->code_message}}</span></td>
                        </tr>
                        <tr>
                            <th class="active">کاربر</th>
                            <td><a href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}">{{$partner->name}}</a></td>

                            <th class="active">زمان رویداد</th>
                            <td class="ltr text-right">{{$creditHolder->created_at_jalali}}</td>
                        </tr>
                        <tr>
                            <th class="active">مبلغ</th>
                            <td><span class="en" dir="ltr">{{price($creditHolder->price)->sep()}}</span> ریال</td>

                            <th class="active">وضعیت پرداخت</th>
                            <td><span class="label label-{{$creditHolder->payment_status_code}}">{{$creditHolder->payment_status_message}}</span></td>
                        </tr>
                    </tbody>
                </table>
                <h3>اطلاعات رویداد</h3>
                @include('admin.partner-credit.events.event-'.$creditHolder->event()->eventCode())
            </div>
        </div>
    </div>
</div>
@stop
