<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            array('state_id' => 1, 'name' => 'آذربايجان شرقي'),
            array('state_id' => 2, 'name' => 'آذربايجان غربي'),
            array('state_id' => 3, 'name' => 'اردبيل'),
            array('state_id' => 4, 'name' => 'اصفهان'),
            array('state_id' => 5, 'name' => 'البرز'),
            array('state_id' => 6, 'name' => 'ايلام'),
            array('state_id' => 7, 'name' => 'بوشهر'),
            array('state_id' => 8, 'name' => 'تهران'),
            array('state_id' => 9, 'name' => 'چهارمحال بختياري'),
            array('state_id' => 10, 'name' => 'خراسان جنوبي'),
            array('state_id' => 11, 'name' => 'خراسان رضوي'),
            array('state_id' => 12, 'name' => 'خراسان شمالي'),
            array('state_id' => 13, 'name' => 'خوزستان'),
            array('state_id' => 14, 'name' => 'زنجان'),
            array('state_id' => 15, 'name' => 'سمنان'),
            array('state_id' => 16, 'name' => 'سيستان و بلوچستان'),
            array('state_id' => 17, 'name' => 'فارس'),
            array('state_id' => 18, 'name' => 'قزوين'),
            array('state_id' => 19, 'name' => 'قم'),
            array('state_id' => 20, 'name' => 'كردستان'),
            array('state_id' => 21, 'name' => 'كرمان'),
            array('state_id' => 22, 'name' => 'كرمانشاه'),
            array('state_id' => 23, 'name' => 'كهكيلويه و بويراحمد'),
            array('state_id' => 24, 'name' => 'گلستان'),
            array('state_id' => 25, 'name' => 'گيلان'),
            array('state_id' => 26, 'name' => 'لرستان'),
            array('state_id' => 27, 'name' => 'مازندران'),
            array('state_id' => 28, 'name' => 'مركزي'),
            array('state_id' => 29, 'name' => 'هرمزگان'),
            array('state_id' => 30, 'name' => 'همدان'),
            array('state_id' => 31, 'name' => 'يزد')
        ];
        DB::table('states')->insert($states);
    }
}
