<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Slider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('slider', function(Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->boolean('status')->default(0);
		    $table->timestamps();
	    });

	    Schema::create('slider_image', function(Blueprint $table) {
		    $table->increments('id');
		    $table->integer('slider_id')->unsigned();
		    $table->string('title');
		    $table->string('name');
		    $table->timestamps();
	    });

	    Schema::table('slider_image', function($table) {
		    $table->foreign('slider_id')->references('id')->on('slider')->onDelete('cascade')->onUpdate('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("SET foreign_key_checks = 0");
	    Schema::dropIfExists('slider');
	    Schema::dropIfExists('slider_image');
	    DB::statement("SET foreign_key_checks = 1");
    }
}
