<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // password_resets
        Schema::create('password_resets', function(Blueprint $table)
        {
            $table->string('type')->index();
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });

        // states
        Schema::create('states', function(Blueprint $table)
        {
            $table->increments('state_id');
            $table->string('name');
        });

        // cities
        Schema::create('cities', function(Blueprint $table)
        {
            $table->increments('city_id');
            $table->string('name');
            $table->integer('state_id')->unsigned();
        });

        // admins
        Schema::create('admins', function(Blueprint $table)
        {
            $table->increments('admin_id');
            $table->string('name');
            $table->string('last_name')->nullable()->default(null);
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('mobile', 11)->unique()->nullable()->default(null);
            $table->string('avatar')->nullable()->default(null);
            $table->string('password', 60);
            $table->boolean('status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        // partners
        Schema::create('partners', function(Blueprint $table) {
            $table->increments('partner_id');
            $table->string('name');
            $table->string('last_name')->nullable()->default(null);
            $table->integer('credit')->default(0);
            $table->mediumInteger('minimum_credit')->default(0);
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('mobile', 11)->unique()->nullable()->default(null);
            $table->string('avatar')->nullable()->default(null);
            $table->char('api_key', 26);
            $table->string('password', 60);
            $table->boolean('status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        // partners_ip_restrict
        Schema::create('partners_ip_restrict', function(Blueprint $table) {
            $table->ipAddress('ip');
            $table->integer('partner_id')->unsigned();
        });

        // partners_credit_events
        Schema::create('partners_credit_events', function(Blueprint $table) {
            $table->increments('partner_credit_event_id');
            $table->smallInteger('code')->unsigned();
            $table->integer('price')->default(0);
            $table->text('description')->nullable()->default(null);
            $table->boolean('payment')->default(0);
            $table->timestamps();
            $table->integer('partner_id')->unsigned();
            $table->integer('related_id')->unsigned()->nullable();
        });

        // partners_users
        Schema::create('partners_users', function(Blueprint $table) {
            $table->increments('partner_user_id');
            $table->string('name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('email')->nullable()->default(null)->unique();
            $table->string('mobile', 11);
            $table->boolean('gender')->nullable()->default(null);
            $table->string('code_meli', 10)->nullable()->default(null)->unique();
            $table->string('state')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->date('birth_date')->nullable()->default(null);
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('partner_id')->unsigned();
        });

        // users
        Schema::create('users', function(Blueprint $table) {
            $table->increments('user_id');
            $table->string('name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('email')->nullable()->default(null)->unique();
            $table->string('mobile', 11)->nullable()->default(null);
            $table->boolean('gender')->nullable()->default(null);
            $table->string('password', 60);
            $table->string('code_meli', 10)->nullable()->default(null)->unique();
            $table->string('state')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->date('birth_date')->nullable()->default(null);
            $table->boolean('status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        // festivals
        Schema::create('festivals', function(Blueprint $table) {
            $table->increments('festival_id');
            $table->string('name');
            $table->string('codename')->unique();
            $table->mediumInteger('code_price')->unsigned()->default(0);
            $table->integer('code_count_max')->unsigned()->nullable()->default(null);
            $table->boolean('status')->default(0);
            $table->timestamps();
        });

        // festivals_codes
        Schema::create('festivals_codes', function(Blueprint $table) {
            $table->increments('festival_code_id');
            $table->string('code');
            $table->boolean('active')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('partner_user_id')->unsigned();
            $table->integer('partner_id')->unsigned();
            $table->integer('festival_id')->unsigned();
        });

        // festivals_partners
        Schema::create('festivals_partners', function(Blueprint $table) {
            $table->integer('partner_id')->unsigned();
            $table->integer('festival_id')->unsigned();
        });

        // settings
        Schema::create('settings', function(Blueprint $table) {
            $table->increments('setting_id');
            $table->string('code');
            $table->string('value')->nullable()->default(null);
            $table->boolean('major')->default(0);
        });

        // settings_text
        Schema::create('settings_text', function(Blueprint $table) {
            $table->increments('setting_text_id');
            $table->string('code');
            $table->text('value');
            $table->boolean('major')->default(0);
        });

        // poolport_transactions
        Schema::create('poolport_transactions', function(Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('port_id')->unsigned();
            $table->decimal('price', 15, 2);
            $table->string('ref_id')->nullable()->default(null);
            $table->string('tracking_code', 50)->nullable()->default(null);
            $table->string('cardNumber', 50)->nullable()->default(null);
            $table->boolean('status')->default(0);
            $table->integer('payment_date')->nullable()->default(null);
            $table->integer('last_change_date');
        });

        // poolport_status_log
        Schema::create('poolport_status_log', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->string('result_code', 10)->nullable()->default(null);
            $table->string('result_message', 255)->nullable()->default(null);
            $table->integer('log_date');
        });

        // report_sms_sends
        Schema::create('report_sms_sends', function(Blueprint $table) {
            $table->smallInteger('job_id')->default(0);
            $table->string('job_desc')->nullable()->default(null);
            $table->string('sms_text');
            $table->string('mobile');
            $table->smallInteger('related_code')->nullable()->default(null);
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->integer('related_id')->unsigned()->nullable()->default(null);
        });

        /****Foreign Keys****/
        // cities
        Schema::table('cities', function($table) {
            $table->foreign('state_id')->references('state_id')->on('states')->onDelete('cascade')->onUpdate('cascade');
        });

        // partners_ip_restrict
        Schema::table('partners_ip_restrict', function($table) {
            $table->foreign('partner_id')->references('partner_id')->on('partners')->onDelete('cascade')->onUpdate('cascade');
        });

        // partners_credit_events
        Schema::table('partners_credit_events', function($table) {
            $table->foreign('partner_id')->references('partner_id')->on('partners')->onDelete('cascade')->onUpdate('cascade');
        });

        // partners_users
        Schema::table('partners_users', function($table) {
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('partner_id')->references('partner_id')->on('partners')->onDelete('cascade')->onUpdate('cascade');
        });

        // festivals_codes
        Schema::table('festivals_codes', function($table) {
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('partner_user_id')->references('partner_user_id')->on('partners_users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('partner_id')->references('partner_id')->on('partners')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('festival_id')->references('festival_id')->on('festivals')->onDelete('cascade')->onUpdate('cascade');
        });

        // festivals_partners
        Schema::table('festivals_partners', function($table) {
            $table->foreign('partner_id')->references('partner_id')->on('partners')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('festival_id')->references('festival_id')->on('festivals')->onDelete('cascade')->onUpdate('cascade');
        });

        // poolport_status_log
        Schema::table('poolport_status_log', function($table) {
            $table->foreign('transaction_id')->references('id')->on('poolport_transactions')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET foreign_key_checks = 0");
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('states');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('admins');
        Schema::dropIfExists('partners');
        Schema::dropIfExists('partners_ip_restrict');
        Schema::dropIfExists('partners_credit_events');
        Schema::dropIfExists('partners_users');
        Schema::dropIfExists('users');
        Schema::dropIfExists('festivals');
        Schema::dropIfExists('festivals_codes');
        Schema::dropIfExists('festivals_partners');
        Schema::dropIfExists('settings');
        Schema::dropIfExists('settings_text');
        Schema::dropIfExists('poolport_transactions');
        Schema::dropIfExists('poolport_status_log');
        Schema::dropIfExists('report_sms_sends');
        DB::statement("SET foreign_key_checks = 1");
    }
}
