<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';

    public $timestamps = true;

    public function slider_image()
    {
        return $this->hasMany('Sehramiz\Models\SliderImage',  'slider_id', 'id');
    }
}
