<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;

class FestivalCode extends Model
{
    protected $table = 'festivals_codes';

    protected $primaryKey = 'festival_code_id';

    public $timestamps = false;
}
