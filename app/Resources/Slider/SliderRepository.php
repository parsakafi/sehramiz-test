<?php

namespace Sehramiz\Resources\Slider;

use Auth;
use DateTime;
use Sehramiz\Models\Slider;
use Sehramiz\Models\SliderImage;
use Illuminate\Http\Request;
use Sehramiz\Resources\Repository;
use Illuminate\Support\Facades\Lang;
use Illuminate\Database\DatabaseManager;

class SliderRepository extends Repository {
	public function __construct
	(
		DatabaseManager $db,
		Slider $slider,
		Request $request
	) {
		$this->request = $request;
		$this->model   = $slider;
		$this->db      = $db;
	}

	/**
	 * New festival from admin
	 *
	 * @return Sehramiz\Models\Slider
	 */
	public function newSlider() {
		$this->beginTransaction();

		$slider         = new $this->model;
		$slider->name   = $this->request->get( 'name' );
		$slider->status = $this->request->get( 'status' );
		$slider->save();

		return $slider;
	}

	public function newImages( $slider_id ) {
		$slider_images = array();
		if ( is_array( $this->request->get( 'image_title' ) ) ) {
			$image_title = $this->request->get( 'image_title' );
			foreach ( $image_title as $k => $title ) {
				if ( $this->request->hasFile( 'image_file.' . $k ) ) {
					if ( $this->request->file( 'image_file.' . $k )->isValid() ) {
						$file = $this->request->file( 'image_file.' . $k );
						$name = time() . rand( 500, 2000 ) . rand( 3000, 5000 ) . '.' . $file->getClientOriginalExtension();
						$this->request->file( 'image_file.' . $k )->move( "uploads", $name );

						if ( ! empty( $title ) ) {
							$slider_image            = new SliderImage;
							$slider_image->title     = $title;
							$slider_image->slider_id = $slider_id;
							$slider_image->name      = $name;

							$slider_images[] = $slider_image;
						}
					}
				}
			}
		}

		return $slider_images;
	}


	/**
	 * Update festival from admin
	 *
	 * @return Sehramiz\Models\Festival
	 */
	public function update( $sliderId ) {
		$this->beginTransaction();

		$slider         = $this->model->find( $sliderId );
		$slider->name   = $this->request->get( 'name' );
		$slider->status = $this->request->get( 'status' );
		$slider->save();

		return $slider;
	}

	/**
	 * Transaction
	 */
	public function transaction( $slider, $images = [] ) {
		try {
			if ( is_array( $images ) && count( $images ) ) {
				foreach ( $images as $image ) {
					$image->save();
				}
			}

			$this->commit();

			return $slider;
		} catch ( \PDOException $e ) {
			$this->rollback();
			throw $e;
		}
	}
}
