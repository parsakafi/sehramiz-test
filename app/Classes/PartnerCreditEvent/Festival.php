<?php

namespace Sehramiz\Classes\PartnerCreditEvent;

use Sehramiz\Models\Festival as FestivalModel;

class Festival extends BaseEvent
{
    protected function loadRelated()
    {
        $this->festival = FestivalModel::find($this->event->related_id);
    }

    public function festivalName()
    {
        return $this->festival->name;
    }

    public function festivalId()
    {
        return $this->festival->festival_id;
    }
}
