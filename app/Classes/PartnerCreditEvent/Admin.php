<?php

namespace Sehramiz\Classes\PartnerCreditEvent;

use Sehramiz\Models\Admin;

class Admin extends BaseEvent
{
    protected function loadRelated()
    {
        $this->admin = Admin::find($this->event->related_id);
    }

    public function adminId()
    {
        return $this->admin->admin_id;
    }

    public function adminName()
    {
        return $this->admin->name;
    }
}
