<?php

namespace Sehramiz\Classes;

use Date\Date;

/**
 * For models have timestamps
 */
trait EloquentTimestampJalaliTrait
{
    /**
     * Payment status message
     *
     * @return string
     */
    public function getCreatedAtJalaliAttribute()
    {
        return (new Date($this->created_at))->toj()->fa();
    }

    /**
     * Payment status code such as success or danger
     *
     * @return string
     */
    public function getUpdatedAtJalaliAttribute()
    {
        return (new Date($this->updated_at))->toj()->fa();
    }
}
