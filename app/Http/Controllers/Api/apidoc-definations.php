<?php

/**
 * @apiDefine Authenticate
 *
 * @apiParam {String} api_key API Key of client for authenticate it
 *
 * @apiErrorExample {json} Unauthorized
 *
 *      HTTP/1.1 401 Unauthorized
 *      {
 *          "status": 0,
 *          "error": "unauthorized"
 *      }
 */

/**
 * @apiDefine Festival
 *
 * @apiParam {String} festival_code Festival code
 *
 * @apiErrorExample {json} Festival not found
 *
 *      HTTP/1.1 400 Bad Request
 *      {
 *          "status": 0,
 *          "error": "festival_not_found"
 *      }
 */

/**
 * @apiDefine Validation
 *
 * @apiParam {String} festival_code Festival code
 *
 * @apiErrorExample {json} Validation error
 *
 *      HTTP/1.1 400 Bad Request
 *      {
 *          "status": 0,
 *          "error": "validation",
 *          "fields": {
 *              "some_field": [
 *                  "Error message"
 *              ]
 *          }
 *      }
 */
