<?php
namespace Sehramiz\Http\Controllers\Api;
use Sehramiz\Http\Controllers\Controller;
use Sehramiz\Models\Slider;
use Sehramiz\Models\SliderImage;

class SliderController extends Controller {
	public function getSliders() {
		$result = Slider::where( 'status', 1 )->select( 'id', 'name' )->first();

		return response()->json( $result, 200 );
	}

	public function getSlider( $id ) {
		$result = array();
		$slide  = Slider::where( 'status', 1 )->where( 'id', $id )->select( 'id', 'name' )->first();
		if ( $slide ) {
			$images         = SliderImage::where( 'slider_id', $id )->get();
			$result['id']   = $id;
			$result['name'] = $slide->name;
			foreach ( $images as $image ) {
				$result['images'][] = array(
					'title' => $image->title,
					'url'   => asset( 'uploads/' . $image->name )
				);
			}
		}

		return response()->json( $result, 200 );
	}
}
