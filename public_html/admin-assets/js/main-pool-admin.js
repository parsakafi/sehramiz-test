$(function() {
    function startTime() {
        $.get(window.BASE_URL + '/dashboard/time', function(time) {
            $('#head-time').text(time);
        });

        t = setTimeout(function () {
            startTime()
        }, 20000);
    }
    startTime();
});
